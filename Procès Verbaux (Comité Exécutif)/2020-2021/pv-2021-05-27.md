# Procès verbal du Conseil Exécutif de l'AGEEI du 27 mai 2020

## Ordre du Jour

1. Ouverture de la réunion
2. Adoption de l'ordre du jour
3. Trésorier
4. V.-p. interne
   1. Barèmes de notation
   2. Fusion avec électro (Louis Meyssonnier)
5. V.-p. aux technologies
   1. Slack -> discord
   2. Transfert des credz
   3. Plateforme pour aider étudiants à collaborer sur des projets personnels
   4. Site web - Organisation du drive
   5. Plateforme de plainte
6. V.-p. compétition
7. V.-p. loisir
   1. Accueil / Intégration / Parrainage / Marrainage
   2. Initiations (et Cle USB initiation 2021)
8. Varia
   1. Objectifs et Grandes Lignes Directrices de l'asso pour l'année 2021-2022
   2. Comité de programme (bac et certif)
   3. Faire le ménage dans les issues
   4. Délai remboursement des cours
9. Fermeture

## Ouverture de la réunion

Fanny propose d'ouvrir la réunion

La proposition est adoptée à l'unanimité

## Adoption de l'ordre du jour

L'ordre du jour est adoptée à l'unanimité

## V.-p. interne

### Barêmes de notation

Armand propose de demander un zoom avec la direction d'ici le 9 juin, pour leur
annoncer qu'on veut avoir un comité de programme et une discussion avec eux sur
le sujet.

La proposition est adoptée à l'unanimité

### Fusion avec électro (Louis Meyssonier)

Armand propose qu'électro nous fournisse une personne-ressource pouvant
participer à nos CE. Tous les évènements de L'AGEEI seront communiqué à cette
personne-ressource. Un accès au slack sera offert. Tout évènement payant sera
facturé au prorata des membres de chaque association.

La proposition est adoptée à l'unanimité

## V.-p. aux technologies

### Slack -> discord

Armand propose de revenir sur le sujet lorsque Simon Désormeaux sera présent.

La proposition est adoptée à l'unanimité

### Transfert des credz

Armand propose que Lancelot s'informe sur l'adoption d'un outil gérer les mots
de passe d'ici la prochaine instance.

La proposition est adoptée à l'unanimité

### Plateforme pour aider les étudiants à collaborer sur des projets personnels

Fanny propose de créer un channel slack et de communiquer autour de ça.

La proposition est adoptée à l'unanimité

### Site web

Pierre-Olivier propose de nommer Armand comme gestionnaire officiel du site web,
et lui suggérant d'arriver avec un site web fonctionnel d'ici à la session
d'automne.

La proposition est adoptée à l'unanimité

## Organisation du drive

Lancelot propose de nettoyer le drive.

La proposition est adoptée à l'unanimité

## Plateforme de plainte

Pierre-Olivier propose qu'au début de chaque session on fournisse aux étudiants
une adresse courriel pour envoyer des plaintes et une marche à suivre pour
formuler des plaintes.

La proposition est adoptée à l'unanimité

## V.-p. loisir

Les points concernant le V.-p. loisir seront revues lorsque Simon Desormeaux
sera présent

## Varia
