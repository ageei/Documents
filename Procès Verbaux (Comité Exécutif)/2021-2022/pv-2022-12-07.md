# Procès verbal de l'assemblée générale de l'AGEEI du 7 décembre 2022

## Ordre du Jour

1. Ouverture de l’assemblée
2. Adoption de l’ordre du jour
3. Adoption du dernier procès-verbal (fourni en pièce-jointe)
4. Avis de motion, modifications des règlements généraux
5. Présentation des états financiers
6. Financement ElleCode&STIM
7. Elections
   1. Elections VP aux affaires internes
   2. Elections VP aux affaires externes
   3. Elections du secrétariat
8. Varia
9. Fermeture de l’assemblée

## 1. Ouverture de l’assemblée

Proposition d'ouverture à 12h52.
Adoptée à l'unanimité.

## 2. Adoption de l’ordre du jour

Proposition d'adoption de l'ordre du jour.
Adoptée à l'unanimité.

## 3. Adoption du dernier procès-verbal (fourni en pièce-jointe)

Proposition d'adoption du dernier procès verbal.
Adoptée à l'unanimité.

## 4. Avis de motion, modifications des règlements généraux

### Reformuler Mode de paiement

Propositon de reformuler la clause "Mode de paiement" de la charte.

La clause se lit présentement :
"
Les dépenses monétaires votées par le comité exécutif sont payées en espèces ou par chèque, selon le cas. Exceptionnellement, une dépense monétaire peut être payée à l'aide de la carte de crédit d'un membre de l'exécutif, notamment dans le cas d'achats en ligne. Une pièce justificative est requise pour chaque dépense.
"

La clause deviendrait :
"
Les dépenses monétaires votées par le comité exécutif sont payées en espèces, par chèques, par carte de débit, par carte de crédit ou par virement Interac. Exceptionnellement, une dépense monétaire peut être payée à partir du compte d'un membre de l'exécutif et le trésorier ou la trésorière procédera au remboursement par virement Interac. Une pièce justificative est requise pour chaque dépense.
"

Appuyée.
Adopté à l'unanimité.

### Ajout section Pouvoir de l'Assemblée Générale

Je, Kim Joziak, membre de l’AGEEI, fait l’avis de motion suivant au sujet de la modification de la charte de l’AGEEI.

CONSIDÉRANT QUE l’allocation du budget de l’association revient à l’assemblée générale et qu’il revient au comité exécutif de respecter cette allocation;

QUE l’on ajoute la section «Pouvoirs de l’Assemblée Générale» qui se lira comme suit:

- L’assemblée générale est l’instance qui forme les résolutions des activités de l’association elle a entre autre les pouvoirs :
  - De décider de l’allocation du budget annuel.
  - De décider de l’offre des activités ou de nommer des comités
  - D’élire ou démettre les fonctions des exécutantes de l’association.
- Lorsque les prévisions budgétaires ont déjà été adoptées pour l’année en cours, toute modification de plus de 500$ doit être faite par avis de motion et votée en assemblée générale

#### Amendement #1

Proposition d'amendé la propositon pour ajouter que toute modification budgétaire ne dépasse pas le budget maximum alloué pour l'année.

Amendement appuyé.

Adopté à majorité.

#### Principale

Appuyé.

Adopté à majorité.

## 5. Présentation des états financiers

Présentation des états financiers

## 6. Financement ElleCode&STIM

Proposition de financer ElleCode&STIM au montant de 1000$.

Appuyé.

Adopté à majorité.

Proposition de prendre l'argent des surplus.

Appuyé.

Adopté à majorité.

## 7. Elections

### 7.1. Elections VP aux affaires internes

Aucune candidature.

### 7.2. Elections VP aux affaires externes

Candidature de Hakim.

### 7.3. Elections du secrétariat

Candidature de Renzo Salcedo.

Candidature de Carl-William Bilodeau-Savaria.

Renzo Salcedo est élu à majorité.

## 8. Varia

- Discord

## 9. Fermeture de l’assemblée

Proposition de fermeture.

Appuyé.

Adopté à l'unanimité.
