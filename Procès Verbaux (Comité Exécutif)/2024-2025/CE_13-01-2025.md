# CE DU 13 JANVIER 2025

## 1 Ouverture

- Ouverture du CE à 18h06.

## 2 Lecture et adoption de l'ordre du jour

- Ordre du jour approuvée et adoptée à l'unanimité.

## 2.1 Whats up

## 3 Nomination des adjoints

- Dany nomine Nicholas Cantin comme adjoint aux compétitions.
- Sublime nomine Elouan Hildgen-Poirier comme adjoint aux technologies.

## 4 Planification de l'AG spéciale de demande d'ammendement à la charte

## 4.1 Formulation des ammendements

- Amendement à la charte 1 :
  - Ajout d'un vote fermé automatique pour les élections en AG. Dans la section 'Vote', on ajoute 'Dans le cas d'une élection, le vote sera automatiquement fermé.'
  - Appuyé et adopté à l'unanimité.

- Amendement à la charte 2 :
  - La phrase 'Le titre de vice-président est octroyé à un autre des vice-présidents.' sera remplacé par 'Le titre de vice-président est octroyé à un autre membre du comité exécutif.'
  - Appuyé et adopté à l'unanimité.

- Amendement à la charte 3 :
  - Ajout d'une section 'Plainte officielle' qui stipule que 'Tout membre de l'association peut déposer une plainte officelle via un formulaire envoyé au VP Interne ou au Président dans le cas ou le VP Interne est la personne concernée, ils auront préalablement signé une entente de non-divulgation pour la prise en charge des dossiers.'
  - Appuyé et adopté à l'unanimité.

- Ajout à l'amendement 3
  - Non-divulgation à la personne visée lorsque le cas pourrait porter atteinte à l'anonymat et la sécurité de la personne qui porte plainte (violence verbale tel que l'intimidation ou l'harcèlement, violence physique, agression sexuelle, etc.)
  - Appuyé et adopté à l'unanimité.

- Amendement à ce dernier ajout :
  - Ajouter la phrase suivante : 'Selon la gravité ou le nombre de plainte, la personne en charge du dossier peut décider de rapidement demander un CE spécial pour mettre en marche le processus de suspension.'
  - Appuyé et adopté à l'unanimité.

- Amendement à la section 'Suspension' de la charte :
  - 'Dans le cas où une plainte est faite sur un membre de l'exécutif, une suspension de trois semaines aura lieu pour prendre le poul sur la/les plainte(s), faire un CE spécial sans la personne visée et ainsi prendre une décision éclairée sur quoi faire avec la personne (destituer ou ne pas destituer).'
  - Appuyé et adopté à l'unanimité.

## 4.2 Choix de la date

- Proposition du 22 janvier 2024 à 12h45.
- Appuyée par Elodie.
- Adoptée à l'unanimité.

## 5 Horaire des activités hiver 2025

## 5.1 Lan party

## 5.1.1 Choix des jeux

- 2 tournois par équipe :
  - Rocket League
  - Overcooked
- 3 jeux individuels :
  - Among Us
  - Mario Party
  - Mario Cart
- Les gens peuvent également apporter leurs propres jeux.
- Appuyé par Sublime
- Adopté à l'unanimité.

## 5.1.2 Choix de la date

- Proposition du vendredi 7 février 2024.
- Dûment appuyée.
- Adoptée à l'unanimité.

## 5.2 Party Post-Intra

- Date : Environ le 14 mars, un vendredi soir.

## 5.3 Cabane à sucre

- Date : Début avril.

## 6 CS Games

## 6.1 Nombre d'équipes

- 2 équipes de 10 personnes avec un mentor par équipe.

## 6.2 Merch pour les participants

- En attente du budget utilisé pour les autobus.

## 6.3 Format des qualifications

- Challenges CTF fait, il reste a créer les autres challenges.
- Cette année, c'est selon les résultats aux challenges des qualifications et la motivation des participants.
- Les qualifications devraient commencer le 28 janvier environ.

## 7 Varia

- Federico a contacté Voltaic pour les photos des finissants et il a rendez-vous avec eux la semaine prochaine. On pourrait reprendre le même contrat que l'an passé. À suivre avec plus de détails.
- Site Web : Page d'accueil sera changé, ajouts à faire (section qui présente les activités à venir et la dernière activité organisé, une section newsletters, une section pour nos partenaires et une pour nos contacts).
- Plan de commandite : Important de prendre en compte les types de commanditaires qu'on peut avoir selon le type d'activité.

## 8 Fermeture

- Fermeture du CE à 20h12.
