# CE DU 21 NOVEMBRE 2024

## 1 Ouverture

- Ouverture du CE à 19h37.

## 2 Choix du président de la séance

- La présidente reste la personne qui préside le CE.
- Choix unanime.

## 3 Lecture et adoption de l'ordre du jour

- Appuyé par Elodie, adopté à l'unanimité.

## 4 Retour sur le CE spécial

- Fournir un document à remplir pour faire une plainte officielle ?
  - Oui, le lien qui mène au formulaire de plainte et serait dans la section 'Questions fréquentes'.
- Est-ce que vous acceptez l'anonymat. Si oui comment vous allez faire pour que cela reste Anonyme ?
  - Anonymat partiel. Le VP Interne recevra les plaintes contre l'AGEEI et ses membres et les plaintes contre le VP Interne seront envoyées.
- Les plaintes doivent être directement envoyées à qui ?
  - Au VP Interne ou à la présidente contre le VP Interne.
- Peut-être mettre des lois qui oblige le VP Interne à garder confidentiel les plaintes afin de ne pas causer des répercussions et des rumeurs ?
  - **Proposition 1 d'amendement à la charte** :
    - Faire signer un contrat électroniquement au VP Interne et Président(e) pour garantir l'anonymat en cas de plainte.
- Après combien de plaintes vous commencez à destitués ou prendre des mesures disciplinaires sur un élu ?
  - **Proposition 2 d'amendement à la charte** :
    - Ajout d'une suspension de trois semaines pour prendre le poul sur la/les plainte(s), faire un CE spécial sans la personne et ainsi prendre une décision éclairer sur quoi faire (destituer ou ne pas destituer) avec la personne.
- En cas de commentaires négatifs contre un candidat, est-ce qu'on devrait faire de base un vote fermée aux AG d'élections ?
  - Oui, formulaire privé en ligne qui prend le nom des personnes présentes. Pas de QR code.
  - Attendre d'avoir plus d'information de la part d'Amélie avant de faire un amendement à la charte pour ce point. Marianne s'en occupe pour voir nos options de vote hybride. Elle va aussi demander à Amélie si elle pourrait être notre compteuse de vote impartiale lors de la prochaine AG d'élections.
- Est-ce qu'on veut embaucher quelqu'un pour faire le présidium ?
  - Pas pour l'instant. Mais c'est une possibilité pour le futur.

## 5 Discussion sur le plan de commandite

- **Plan de commandite pour AGEEI par Emile**

  - *A. Introduction*
    - Présentation brève de l’association : nom, date de création, objectif principal.
    - Description des activités et événements organisés par l'association.
    - Importance de l’association dans la communauté (par exemple, promouvoir l'informatique et la technologie).

  - *B. Objectifs du partenariat*
    - Objectifs du plan de commandite (par exemple, soutien financier pour des événements, achat de matériel, développement de projets éducatifs, etc.).
    - Bénéfices pour les commanditaires (par exemple, visibilité, accès à une audience ciblée, association à des projets innovants).

  - *C. Description des événements et projets*
    - Liste des événements clés organisés par l’association (par exemple, hackathons, ateliers, conférences).
    - Description des projets en cours ou à venir (projets éducatifs, recherche, développement de logiciels open source, etc.).
    - Public cible et taille estimée de l’audience pour chaque événement ou projet.
      - *1- Ateliers de Programmation*
        - Organise des ateliers interactifs pour enseigner différents langages de programmation et concepts avancés.
          - Python pour les débutants : Initiation à la programmation avec un focus sur la logique et les algorithmes de base
          - Développement web : Ateliers sur HTML, CSS, JavaScript, et les frameworks comme React ou Angular.
          - Développement d’applications mobiles : Créer des applications avec Flutter ou React Native.
          - Ateliers d’intelligence artificielle : Introduction à l'IA, au machine learning avec des outils comme TensorFlow ou PyTorch.
        - Pourquoi cela intéresserait un commanditaire :
          - Les entreprises technologiques recherchent des talents qualifiés, et soutenir la formation dans ces domaines peut les aider à attirer de futurs développeurs.
      - *2- Compétitions de Hackathons*
        - Organise des hackathons thématiques où les participants doivent développer un projet en équipe dans un délai limité (24-48 heures).
          - Hackathon d'IA : Défi autour de l'usage de l'IA dans la santé, l’environnement, ou la finance.
          - Hackathon de développement d’applications : Créer des applications utiles pour la communauté (santé, éducation, transport).
        - Pourquoi cela intéresserait un commanditaire :
          - Les hackathons permettent aux entreprises de découvrir des talents en action. Elles peuvent également sponsoriser des prix, du matériel, ou simplement obtenir de la visibilité.
      - *3- Programme de Cybersécurité*
        - Mettre en place un programme complet pour développer les compétences en cybersécurité, allant des bases aux sujets plus avancés :
          - Ateliers sur les bases de la sécurité informatique : Cryptographie, sécurisation des réseaux, pare-feu, VPN.
          - Simulations de Cyberattaques : Organise des défis de type "Capture The Flag" (CTF), où les participants doivent trouver et exploiter des failles dans des systèmes virtuels.
          - Formation sur l'audit de sécurité et le pentesting : En utilisant des outils comme Kali Linux, Metasploit, et Wireshark.
      - *4- Competition de Gamejam*
        - Pourquoi cela intéresserait un commanditaire :
          - Avec l'augmentation des cyberattaques, les entreprises sont à la recherche de talents qualifiés pour protéger leurs infrastructures numériques. Un soutien à ce programme montre un engagement envers la sécurité et la formation.
      - *5- Formation à la Création de Jeux Vidéo*
        - Propose une série de formations pour initier les membres à la conception et au développement de jeux vidéo.
          - Initiation à Unity et Unreal Engine : Développer des jeux 2D et 3D en utilisant ces moteurs de jeu populaires.
          - Ateliers de design de jeux : Enseigner la création d’univers, de personnages, et la mécanique de jeu (game design).
          - Concours de développement de jeux vidéo : Organise une compétition où les participants créent un jeu en équipe, jugé sur la créativité, la jouabilité et le design.
        - Pourquoi cela intéresserait un commanditaire :
          - L'industrie du jeu vidéo est en pleine croissance, et les entreprises de technologie ou de divertissement pourraient être intéressées par le soutien à la formation dans ce secteur.
      - *6- Bootcamps intensifs en programmation et cybersécurité*
        - Organise des bootcamps d'une semaine ou plus pour des sessions intensives sur un domaine spécifique.
          - Bootcamp Full-stack : Formation intensive sur le développement web front-end et back-end.
          - Bootcamp de cybersécurité : Focus sur l’analyse des vulnérabilités, la réponse aux incidents, et les tests de pénétration.
        - Pourquoi cela intéresserait un commanditaire :
          - Les entreprises peuvent profiter d'un partenariat pour proposer des formations spécialisées et voir les compétences des participants. Elles peuvent aussi proposer des certifications professionnelles à la fin des bootcamps.
      - *7- Conférences et Séminaires Technologiques*
        - Organise des événements ou des conférences sur des sujets comme la cybersécurité, les jeux vidéo et l’innovation en programmation.
          - Conférences sur la cybersécurité : Inviter des experts à partager les dernières tendances et menaces.
          - Conférences sur le développement de jeux vidéo : Focus sur la narration interactive, la modélisation 3D, ou la réalité augmentée/virtuelle (AR/VR).
        - Pourquoi cela intéresserait un commanditaire :
          - Les entreprises de technologie et de développement de jeux vidéo sont toujours à la recherche de nouvelles idées et talents. En sponsorisant ces événements, elles renforcent leur image auprès de la communauté technologique.
      - *8- Programme de Mentorat*
        - Mets en place un programme de mentorat où des experts en programmation, cybersécurité ou création de jeux vidéo partagent leurs connaissances avec les membres de l’association.
          - Mentorat en développement logiciel : Un soutien pour les projets individuels ou de groupe.
          - Mentorat en cybersécurité : Accompagner les membres dans des simulations ou certifications en cybersécurité.
        - Pourquoi cela intéresserait un commanditaire :
          - Le mentorat montre l’implication des entreprises dans la formation de la prochaine génération de professionnels tout en créant des liens directs avec des talents prometteurs.

  - *D. Opportunités de commandite*
    - Niveau Platine : Inclut un soutien majeur (montant élevé) et offre une visibilité maximale (par exemple, logo sur les affiches, mentions dans les discours, visibilité sur le site web, etc.).
    - Niveau Or : Soutien intermédiaire avec une visibilité importante (par exemple, logo sur le matériel promotionnel, mention lors des événements).
    - Niveau Argent : Soutien modéré avec des avantages de visibilité plus limités.
    - Niveau Bronze : Contribution de base avec des mentions simples sur les supports numériques et les brochures.

  - *E. Avantages pour les commanditaires*
    - Détails des avantages spécifiques pour chaque niveau de commandite (par exemple, mentions sur les réseaux sociaux, invitations VIP à des événements, opportunités de marketing).
    - Possibilité de personnaliser certaines offres pour répondre aux besoins spécifiques des commanditaires.

  - *F. Budget et besoins financiers*
    - Détail des besoins financiers spécifiques pour les événements et projets.
    - Justification des montants demandés selon les niveaux de commandite (par exemple, coût du matériel, logistique, rémunération des intervenants).

  - *G. Visibilité et marketing*
    - Stratégie de promotion pour les événements (par exemple, réseaux sociaux, affiches, newsletters).
    - Importance de la visibilité du commanditaire dans ces canaux de communication.

  - *H. Contact*
    - Informations de contact (nom, email, téléphone) pour les entreprises intéressées par la commandite.

  - *Annexes*
    - Calendrier des événements pour l’année.
    - Exemples de matériel promotionnel des éditions précédentes (si disponible).
    - Témoignages d’anciens commanditaires ou partenaires.

- **Notes supplémentaires lors du CE**
  - Les commanditaires aiment qu'on ait un photographe qui prend des photos de bonne qualité pour nos évènements.
    - À voir si on veut embaucher quelqu'un ou si on peut mettre quelqu'un responsable à l'interne.

  - *Optimiser LinkedIn, Instagram, Facebook et notre site web.*
    - Instagram : recap du mois, capsule vidéo de Dany qui parle des compés, etc. pour améliorer notre visibilité.
    - Concours pour ceux qui nous follow. Petite carte-cadeau de 20$ par exemple.
    - LinkedIn : Follow et publication pour les commandites, étudiants pour promouvoir auprès des employeurs.

  - *Ordre de travail :*
    - 1- Site web
      - Calendrier mis à jour pour toutes les activités voté au dernier AG.
      - Ajouter tous les membres
      - Ajouter une page pour la marchandises ? Mettre un message 'À venir'
      - Ajouter une section 'Gallerie' avec les photos séparés par évènement
    - 2- LinkedIn
    - 3- Instagram

## 6 Update escalade

- 20 inscriptions jusqu'à présent. 40 billets de payés à l'avance. Pas de privatisation.
- On va payer 10 billets supplémentaires si le nombre d'inscriptions dans le zeffy atteint 40.
- Il faut faire signer un engagement de responsabilité à tous les membres avant l'événement.
- Formulaire de déresponsabilité pour l'AGEEI ou mention dans nos prochaines communications.
- Carl-William est là de quel heure à quel heure ? Carl-William sera la de 17h à 17h40.
- Qui prend sa relève et s'occupe de l'activité à son départ ? Emile !
- Qui de l'exec participe ? Badr, Emile (en retard), Federico, Sublime, Jean-Christophe.

## 7 Varia

- **Proposition 1**
  - Badr propose qu'on achète une cafetière à pods pour que chaque étudiants puissent ramener les leur.
    - Bring your own pods. Bring your own milk. La cafetière reste dans le frigo.
  - Amendement d'Elodie : L'acheter sur Marketplace et mettre un maximum de 50$.
  - Amendement de Marianne : Prendre l'argent du budget Midi-Psycho puisque c'est annulé.
  - Vote Keurig ou Nespresso Vertuo ? Nespresso Vertuo gagne !
  - Adopté à l'unanimité.
- Acheter un cadenas à code pour le frigo. Federico s'en charge.
- Magasiner une housse pour le divan 2 places (+ une pour le divan dans le local de l'exec possiblement). Federico s'en charge.
- Point d'Elodie
  - Important de mettre en place une manière pour choisir les candidats aux compétitions qui sont payés avec les cotisations des étudiants de l'AGEEI.
- Point d'Emile
  - Est-ce qu'on pourrait avoir un abonnement Canva ? Voir avec Kelly comment elle avait fait pour ElleCode.
  - Organiser des 'midi-café' ou 'midi-partage' pour favoriser les discussions et le partage de connaissances.
    - Implique l'achat de carafes de cafés : 150$ restant du budget '' pour acheter des carafes de café.
- VP Exécutif
  - **Proposition 3 d'amendement à la charte** :
    - Proposition de pouvoir nommer un officier de première année à la charte.
    - Adopté à l'unanimité.

## 8 Fermeture

- Fermeture du CE à 21h52.
