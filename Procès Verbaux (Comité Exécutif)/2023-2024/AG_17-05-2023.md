# ASSEMBLÉE GÉNÉRALE DE L'AGEEI DU 17 MAI POUR LA SESSION D'ÉTÉ 2023 (SH-R810)

## Présentation de l'ordre du jour

1. Ouverture
2. Budget
3. Varia
4. Fermeture

## Ouverture

**Proposition 1 :**

- Ouverture de l'assemblée générale;
- Dûment proposée;
- Dûment appuyée;
- L'assemblée est ouverte à 12h37.

## Budget

**Trésorière** :

- On estime la totalité des cotisations à 4000$ maximum pour l'été 2023.

**Proposition 2 :**

Budget d'été

- Budget compétition :

- Pratique de compétition format simple -> 200$ x 4 = 800$
- Pratique de compétition format "Montrehack" -> 300$ x 1 = 300$
- Midi présentation cyber sécurité -> 300$ x 1 = 300$
- Total 1400$

- Budget VP interne :

  - Pour le local (nouvelles ampoules, cafetière bodum) -> 200$

- Budget loisirs :

- Tour à vélo de 25km à Montréal -> 1500$
- Soirée BBQ -> 1000$
- Promenade au Mont-Royal -> 200$

- Total budget été 4300$

- Dûment appuyée et adoptée à majorité.

**Proposition 3 :**

Budget initiations

- Proposée par Carl-William;
- Proposition d'un budget de 11500$ pour les initiations d'automne 2023;
- Dûment appuyé par Armand;
- Jessica demande le vote;
- La proposition est adoptée à l'unanimité.

## Varia

### Demande d'avis de motion

Armand demande d'enlever les ambassadeurs dans la charte.

## Fermeture

**Proposition 4 :**

Fermeture de l'assemblée générale.
Proposée par Kevin;
Dûment appuyée;
Adoptée à l'unanimité.
L'assemblée est levée à 13:10.
