# CE DU 21 AVRIL 2023

## 1 - Lecture et adoption de l'ordre du jour

Proposition 1 :

- Proposition d’ouverture du CE par Alex.
- Dûment appuyée par Kim et ouvert à 17h25.

## 2 - Présentation des adjoint.es

François Beaumier, nouvel adjoint au VP Externe

Alexandre Dumoulin, nouvel adjoint au VP Loisirs

Marianne Boyer, nouvelle adjointe au VP Interne

Stefan Voicu, nouvel adjoint au VP Techno

## 3 - Formation harcèlement

Formation sur la prévention des violences à caractères sexuels :

- Réserver la date pour les nouveaux membres
- Voir si les anciens membres ont besoin de la refaire

## 4 - Élection vice président.e exécutif

Carl-William Savaria-Bilodeau, VP loisirs, est élu vice-président

## 5 - Party de fin de session

Où : La Petite Grenouille sur St-Laurent

Quand : Vendredi le 5 mai

Senti : 2-3 personnes sobres responsablent des étudiants pour la soirée

- 51$ par communauto pour la soirée
- Rémunération?

Billets :

- 10$/billet juste pour les membres de l’AGEEI
- 1 billet donne accès à 5 consommations
- Aucun shooter ou bouteille d’eau contre un ticket (aviser le bar)
- Les non-membres payent leurs propres consommations
- Les membres de l’exécutif ne payent pas leur billet
- Les adjoints/adjointes et les membres du comité loisirs payent leur billet
- Mettre sur les billets “Buvez avec parcimonie, soyez responsable!”

Bar :

- Vestiaire disponible et aucun coût d’entrée
- Coûte 3000$ pour 75 personnes (5 consommations par personne)
- 1000$ de budget pour des shooters (25$ pour 25 shooters)

Utilisation de Zeffy :

- Plateforme complètement gratuite (aucuns frais de transaction)
- S'assurer que la communauté étudiante retire le pourboire de 18% pour ne pas payer des frais supplémentaires

Proposition 2 :

- Armand propose qu'AUCUNS transports ne soient payés par l’AGEEI pour ce party
- Elliot propose le vote.
- Dûment appuyée.
- Adopté à majorité.

## 6 - Comité loisirs

Approbation à l'unanimité des 8 nouveaux membres dans le comité loisirs :

- Carl-William Bilodeau-Savaria
- Samuel Rochon
- Elodie Brunet
- Alexandre Dumoulin
- Étienne Comptois
- Lukas Poulin
- Keven Jude Antenor
- Derick Gagnon

Important de s'assurer que ces membres contactent au nom du comité loisirs (pas au nom de l’AGEEI).

Proposition 3 :

- Guillaume propose qu'un maximum de 10 personnes puissent être au sein du comité.
- Dûment appuyée par William.
- Adoptée à l’unanimité.

## 7 - CTF ageei

Proposition 4 :

- Armand propose l'archivage des challenges CTFD et sauvegarde automatique des BD sur le gitlab de l’AGEEI.
- Armand propose le vote.
- Dûment appuyée.
- Adopté à l'unanimité.

## 8 - Budget été

Faire une AG pour allouer du budget pour l’été (conférences, évènements, compétitions, etc.)

Possibilité budget : (nb d’étudiants inscrits à la session d’été) x 18 = budget alloué

Mado le 24 mai :

- Évènement au Mado organisé par *Ellecode*
- Possibilité de payer la moitié du coût des billets (~14$)

Proposition 5 :

- Alex propose d'annoncer l'AG de mai 14 jours d'avance.
- Dûment appuyée par Elliott.
- Adoptée à l’unanimité.

Proposition 6 :

- Alex propose que le comité exécutif s’engage à discuter d’un budget d’été qui ne dépasse pas 5000$.
- Dûment appuyée par Armand.
- Adoptée à l’unanimité.

## 9 - Calendrier d'événements 2023-2024

Proposition 7 :

- Armand propose que tous les VP donnent leurs dates d’évènements d'au moins une session complète. Elles seront affichées dans un calendrier sur le site internet de l’AGEEI avant septembre (party, activités, évènements, compétitions, midi-conférences, etc.)
- Dûment appuyée par Guillaume.
- Adoptée à l’unanimité.

## 10 - Discussions ambassadeur/drice.s

Qui sont-ils?

- Des personnes diplomées qui promouvoient l’UQAM sur le marché du travail et ramènent du savoir à l’UQAM.
- Ces personnes diplomées sont engagées sur le durée de leur mandat dans la charte.
- On les enlève car ces personnes n’ont plus raison d’être.

## 11 - Clef du local

1 clé à faire du local commun (Samuel).

3 clés à faire du local AGEEI (Samuel, Armand, Elodie).

## 12 - Le courriel <executif@ageei.org>

Donner les accès à tout le comité exécutif.

Le président et la secrétaire s’engagent à regarder les courriels souvent et s’occuper de les transférer aux gens concernés.

## 13 - Gilab – Approval

Proposition 8 :

- Elliott propose de descendre le nombre de membres pour approuver les merges sur le gitlab.
- Dûment appuyée par Kim.
- Adoptée à l'unanimité.

## 14 - Bot discord AGEEI pour l'envoi d'annonces

On va en discuter au prochain AG.

## 15 - Écriture inclusive

L’exécutif s’engage à utiliser l’écriture épiscène (non genrée) dans les communications de l’AGEEI.

- Ex : la communauté étudiante

## 16 - Sélection des participant.e.s Nsec

8 billets disponibles (2 billets CTF-conférence, 6 billets CTF).

Aucun traitement de faveur, pas de qualification sans faire les write-up.

Si une personne sélectionnée se désiste, une autre personne parmis ceux qualifiés sera sélectionnée.

Ceux qui envoient le 26 avril sont plus haut dans la sélection mais jusqu’au 1 mai non-officiellement pour envoyer.

## 17 - Nettoyage du frigo

Proposition 9 :

- Guillaume propose de laver le réfrigérateur chaque semaine, possibilité d’écrire nos noms sur nos repas. Les restants de pizza commandés lors des évènements ne seront pas jetés.
- Dûment appuyée par Elliott.
- Adoptée à l’unanimité.

## 18 - Fermeture

Proposition 10 :

- Alex propose la fermeture du CE.
- Dûment appuyée par Kim.
- Adoptée à l’unanimité.
- CE fermé à 19:54.
