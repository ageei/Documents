# Procès verbal du Conseil Exécutif de l'AGEEI du 18 juin 2020

## Ordre du Jour

1. Ouverture de l'assemblée
2. Adoption de l'ordre du jour
3. Adoption du dernier procès-verbal
4. Nominations
   1. Nomination du/de la président.e
   2. Nomination du/de la secrétaire
   3. Nomination du/de la trésorier/ère
   4. Nomination du/de la VP aux technologies
   5. Nomination du/de la VP aux affaires internes
   6. Nomination du/de la VP aux affaires externes
   7. Nomination du/de la VP aux loisirs
   8. Nomination du/de la VP aux compétitions
   9. Nomination des ambassadeurs
5. Élections des étudiant.e.s au comité de programme du Bac
6. Élections des étudiant.e.s au comité de programme du certificat
7. Présentation des états financiers
8. Photo de finissants
9. Varia
10. Fermeture de l'assemblée

## 1. Ouverture de l'assemblée

Nicolas Papanicolaou propose l'ouverture.
Pierre-Olivier Brillant appuie.
La proposition est adoptée à l'unanimité.

## 2. Adoption de l'ordre du jour

Nicolas Papanicolaou propose d’ajouter un point “photo de finissant”.
Audrey Larivière appuie.
La proposition est adoptée à l'unanimité.

Nicolas Papanicolaou propose l'adoption de l'ordre du jour tel qu'amendé.
Armand Brière appuie.
La proposition est adoptée à l'unanimité.

## 3. Adoption du dernier procès-verbal

Maxime Masson propose d'adopter le procès verbal du 10 octobre 2019.
Armand Brière appuie.
La proposition est adoptée à l'unanimité.

## 4. Nominations

### 4.1. Nomination du/de la président.e

Jean-Philippe Robitaille-Laratt se présente.
Pierre-Olivier Brillant se présente.

Jean-Philippe Robitaille-Laratt est élu à majorité.

### 4.2. Nomination du/de la secrétaire

Pierre-Olivier Brillant se présente.

Pierre-Olivier est élu à majorité.

### 4.3. Nomination du/de la trésorier/ère

Armand Brière se présente.
Harry Obertan se présente.

Armand Brière est élu à majorité.

### 4.4. Nomination du/de la VP aux technologies

Philippe Grégoire se présente.

Philippe Grégoire a été élu à majorité.

### 4.5. Nomination du/de la VP aux affaires internes

Fanny Lavergne se présente.

Fanny Lavergne est élu à majorité.

### 4.6. Nomination du/de la VP aux affaires externes

### 4.7. Nomination du/de la VP aux loisirs

Maxime Masson se présente.

Maxime Masson est élu à majorité.

### 4.8. Nomination du/de la VP aux compétitions

Philippe Van Velzen se présente.

Philippe Van Velzen a été élu à majorité.

### 4.9. Nomination des ambassadeurs

Les ambassadeurs en ce moment sont:

- Kerby Geffrard
- Nicolas Lamoureux
- Nicolas Papanicolaou
- Philippe Pépos Petitclerc
- Marc-Antoine Sauvé
- Marie-Pier Lessard
- Maxime Faubert

Philippe Van Velzen nomine les ambassadeurs suivant:

- Kerby Geffrard
- Nicolas Lamoureux
- Nicolas Papanicolaou
- Philippe Pépos Petitclerc
- Marc-Antoine Sauvé
- Marie-Pier Lessard
- Maxime Faubert

Corinne Pulgar appuie.
Proposition accepté à majorité.

## 5. Élections des étudiant.e.s au comité de programme du Bac

Philippe Grégoire propose de remettre l’élection à plus tard.
Philippe Van Velzen appuie.
La proposition est adoptée à l'unanimité.

## 6. Élections des étudiant.e.s au comité de programme du certificat

Philippe Grégoire propose de remettre l’élection à plus tard.
Armand Brière appuie.

David Thibault-Grenier amende “à la prochaine Assemblée Générale plutôt que “à
plus tard”. Audrey Larivière appuie. La proposition est adoptée à majorité.

Remettre l’élection à la prochaine Assemblée Générale.
La proposition est battue à majorité.

Philippe Grégoire propose que l’élection se fasse par le CE au début de la
session d’automne. Philippe Van Velzen appuie. La proposition est adoptée à
l'unanimité.

## 7. Présentation des états financiers

Armand Brière propose que Harry Obertan présente le bilan financier à la
prochaine assemblée générale. David Thibault-Grenier appuie. La proposition est
adoptée à l'unanimité.

## 8. Photo des finissants

Corinne Pulgar propose que le CE s’engage à s’occuper du dossier des photos des
finissants. Alexandre Coté Cyr appuie. La proposition est adoptée à l'unanimité.

## 9. Varia

## 10. Fermeture de l'assemblée

Nicolas Papanicolaou propose la fermeture de l’assemblée.
David Thibault-Grenier appuie.
La proposition est adoptée à l'unanimité.
