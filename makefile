

MDL=	/usr/local/bundle/bin/mdl -r ~MD002,~MD013,~MD024,~MD003,~MD029,~MD032

check:
	find . -type f -name '*.md' | while read md; do \
		printf '==> %s\n' "$${md}"; \
		${MDL} "$${md}"; \
	done
